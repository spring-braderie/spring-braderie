package cda6.JCV.JB_CV_CF_SpringBraderie.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * POJO article qui correspond à la table T_article de la BDD
 * @author Clément Fraisseix
 *
 */
@Entity
@Table(name="t_article")
public class Article implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idarticle", unique=true, nullable=false)
	private Integer idarticle;
	
	@Column(name="description")
	private String description;
	
	@Column(name="marque")
	private String marque;
	
	@Column(name="prixunitaire")
	private Integer prixunintaire;
	
	@Column(name="quantite")
	private Integer quantite;
	
	public Article() {		
	}

	public Article(Integer idarticle, String description, String marque, Integer prixunintaire, Integer quantite) {
		this.idarticle = idarticle;
		this.description = description;
		this.marque = marque;
		this.prixunintaire = prixunintaire;
		this.quantite = quantite;
	}
	
	public Article(String description, String marque, Integer prixunintaire, Integer quantite) {
		this.description = description;
		this.marque = marque;
		this.prixunintaire = prixunintaire;
		this.quantite = quantite;
	}
	
	public Article(String description, String marque, Integer prixunintaire) {
		this.description = description;
		this.marque = marque;
		this.prixunintaire = prixunintaire;
	}
	
	public Integer getIdarticle() {
		return idarticle;
	}

	public void setIdarticle(Integer idarticle) {
		this.idarticle = idarticle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public Integer getPrixunintaire() {
		return prixunintaire;
	}

	public void setPrixunintaire(Integer prixunintaire) {
		this.prixunintaire = prixunintaire;
	}

	public Integer getQuantite() {
		return quantite;
	}

	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}

	@Override
	public String toString() {
		return "Article [idarticle=" + idarticle + ", description=" + description + ", marque=" + marque
				+ ", prixunintaire=" + prixunintaire + "]";
	}
	
	
}
