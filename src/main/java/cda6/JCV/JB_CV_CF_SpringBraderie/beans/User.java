package cda6.JCV.JB_CV_CF_SpringBraderie.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Rosie
 *
 *
 */

@Entity
@Table(name = "t_user")
public class User {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer iduser;
	@Column(name = "login")
	private String login;
	@Column(name = "pass")
	private String pass;
	@Column(name = "nbconnexion")
	private Integer nbconnexion;
	
	
	
	public User() {
		super();
	}


	
	
	public Integer getIduser() {
		return iduser;
	}


	public void setIduser(Integer iduser) {
		this.iduser = iduser;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getPass() {
		return pass;
	}


	public void setPass(String pass) {
		this.pass = pass;
	}


	public Integer getNbconnexion() {
		return nbconnexion;
	}


	public void setNbconnexion(Integer nbconnexion) {
		this.nbconnexion = nbconnexion;
	}


	@Override
	public String toString() {
		return "User [iduser=" + iduser + ", login=" + login + ", pass=" + pass + ", nbconnexion=" + nbconnexion + "]";
	}
	

}
