package cda6.JCV.JB_CV_CF_SpringBraderie.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;



/**
 * @author jordy
 *
 */
@Entity
@Table(name = "t_panier")
public class Panier implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer idpanier;
	private User user;
	private Article article;
	private Integer quantite;


	public Panier() {
		super();
	}

	public Panier(User user, Article article, Integer quantite) {
		super();
		this.user = user;
		this.article = article;
		this.quantite = quantite;
	}

	public Panier(Integer idpanier, User user, Article article, Integer quantite) {
		super();
		this.idpanier = idpanier;
		this.user = user;
		this.article = article;
		this.quantite = quantite;
	}


	@Override
	public String toString() {
		
		StringBuilder builder = new StringBuilder();
		builder.append("{\"Panier\" :");
        builder.append("{\"idpanier\" :");
        builder.append(idpanier);
        builder.append(", \"user\" :");
        builder.append(user.getLogin());
        builder.append(", \"article\" :");
        builder.append(article);
      //  builder.append(article);
        builder.append(", \"quantite\" :");
        builder.append(quantite);
        builder.append("}");
        builder.append("}");
    return builder.toString();

	}


	/**
	 * @return the idpanier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "idpanier", unique = true, nullable = false)
	public Integer getIdpanier() {
		return idpanier;
	}

	/**
	 * @param idpanier the idpanier to set
	 */
	public void setIdpanier(Integer idpanier) {
		this.idpanier = idpanier;
	}

	/**
	 * @return the user
	 */
	@Column(name = "user")
	@OneToOne
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the article
	 */
	@Column(name = "article")
	public Article getArticle() {
		return article;
	}

	/**
	 * @param article the article to set
	 */
	public void setArticle(Article article) {
		this.article = article;
	}

	/**
	 * @return the quantite
	 */
	@Column(name = "quantite")
	public Integer getQuantite() {
		return quantite;
	}

	/**
	 * @param quantite the quantite to set
	 */
	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}


	public void increaseQuantity() {
		this.quantite++;
	}

}

	

