package cda6.JCV.JB_CV_CF_SpringBraderie.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cda6.JCV.JB_CV_CF_SpringBraderie.beans.User;
import cda6.JCV.JB_CV_CF_SpringBraderie.repository.UserRepository;


@Service
public class UserService {

	
	@Autowired
	private UserRepository userRepository;
	
	private static final Logger log = LoggerFactory.getLogger(UserService.class);
	
	
	
	
	
	public Optional<User> findById(Integer id){
		log.info("findid dans service", userRepository.findById(id));
		return userRepository.findById(id);
	}
	
	
	public Iterable<User> findAllUser() {
		userRepository.findAll().forEach(user -> log.info(user.getLogin()));
		return userRepository.findAll();
	}
	
}
