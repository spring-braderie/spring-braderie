package cda6.JCV.JB_CV_CF_SpringBraderie.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cda6.JCV.JB_CV_CF_SpringBraderie.beans.Article;
import cda6.JCV.JB_CV_CF_SpringBraderie.repository.ArticleRepository;

@Service
public class ArticleService {

	@Autowired
	private ArticleRepository articleRepository;

	/**
	 * Service qui permet de récupèrer le catalogue 
	 * (toutes les lignes de la table t_article)
	 * 
	 * @return Un Iterable
	 */
	public Iterable<Article> getCatalogue(){
		//Appel de la méthode findAll de la classe abstraite CrudRepository pour récupérer la liste des articles
		articleRepository.findAll().forEach(article -> System.err.println(article.getDescription()+" "+article.getMarque()+" "+article.getPrixunintaire()+"€"));
		return articleRepository.findAll();
	}

	/**
	 * Méthode qui permet de supprimer un article via sur id
	 * @param id de l'article à supprimer
	 */
	public void deleteArticleById(Integer id) {
		articleRepository.deleteById(id);
	}

	/**
	 * Méthode qui permet d'ajouter un article 
	 * @param article à ajouter
	 * @return l'article qui a été ajouter
	 */
	public Article addArticle(Article article){
		System.err.println(articleRepository.save(article));
		return articleRepository.save(article);	
	}


	/**
	 * Méthode qui permet de retrouver un article via sur id
	 * On utilise la classe Optional qui permet d'encapsuler un object dont la valeur peut-être null
	 * @param id de l'article à trouver
	 * @return l'article en question
	 */
	public Optional<Article> getArticleById(Integer id) {
		articleRepository.findById(id).ifPresent(theArticle -> System.err.println(theArticle.getDescription()+" "+theArticle.getMarque()+" "+theArticle.getPrixunintaire()));
		return articleRepository.findById(id);
	}
}

