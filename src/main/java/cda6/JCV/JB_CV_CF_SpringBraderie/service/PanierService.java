package cda6.JCV.JB_CV_CF_SpringBraderie.service;



import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cda6.JCV.JB_CV_CF_SpringBraderie.beans.Article;
import cda6.JCV.JB_CV_CF_SpringBraderie.beans.Panier;
import cda6.JCV.JB_CV_CF_SpringBraderie.beans.User;
import cda6.JCV.JB_CV_CF_SpringBraderie.repository.ArticleRepository;
import cda6.JCV.JB_CV_CF_SpringBraderie.repository.PanierRepository;

/**
 * @author jordy
 *
 */
@Service
public class PanierService {

	@Autowired
	private PanierRepository panierrepo;

	@Autowired
	private ArticleRepository articlerepo;


	@Autowired
	private Logger logger;

	/**
	 * 
	 */
	public Integer AjouterPanier(Integer idarticle, User user, Integer quantite) {

		Integer newQte = quantite;
		Article article = articlerepo.findById(idarticle).orElse(null);
		Panier panier = panierrepo.findByArticleAndUser(article, user);

		if (panier != null) {
			newQte = panier.getQuantite() + quantite;
			panier.setQuantite(newQte);
			
		} else {

			panier = new Panier();
			panier.setArticle(article);
			panier.setUser(user);
			panier.setQuantite(quantite);

			panierrepo.save(panier);
			logger.info("Article ajouté au panier avec succès");
		}
		
		return newQte;
	}
	
	/**
	 * @return une liste de panier en lien avec son user
	 */
	public List<Panier> AfficherPanierbyUser(User user) {
		logger.info("Le panier contient : ");
		return panierrepo.findByUser(user);
	}
	
	public void SupprimerPanier(Integer idarticle, User user) {
		panierrepo.removeByArticleAndUser(idarticle, user.getIduser());
	}
	
	public void SupprimerTout() {
		panierrepo.deleteAll();
	}

	

}
