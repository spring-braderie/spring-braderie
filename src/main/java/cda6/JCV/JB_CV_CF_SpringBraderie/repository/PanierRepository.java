package cda6.JCV.JB_CV_CF_SpringBraderie.repository;

import java.util.List;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import cda6.JCV.JB_CV_CF_SpringBraderie.beans.User;
import cda6.JCV.JB_CV_CF_SpringBraderie.beans.Article;
import cda6.JCV.JB_CV_CF_SpringBraderie.beans.Panier;

@Repository
public interface PanierRepository extends CrudRepository<Panier, Integer> {
	
	public List<Panier> findByUser(User user);
	
	public Panier findByArticleAndUser(Article article, User user);
	
	@Query("delete from panier p where p.article.idarticle = ?1 and p.user.iduser = ?2")
	@Modifying
	public void removeByArticleAndUser(Integer idarticle, Integer iduser);
 

}
