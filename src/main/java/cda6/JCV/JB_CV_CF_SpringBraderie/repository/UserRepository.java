package cda6.JCV.JB_CV_CF_SpringBraderie.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cda6.JCV.JB_CV_CF_SpringBraderie.beans.User;


@Repository
public interface UserRepository extends CrudRepository <User, Integer> {

}
