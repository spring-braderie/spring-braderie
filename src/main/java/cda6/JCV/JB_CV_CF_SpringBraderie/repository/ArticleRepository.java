package cda6.JCV.JB_CV_CF_SpringBraderie.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cda6.JCV.JB_CV_CF_SpringBraderie.beans.Article;

@Repository
public interface ArticleRepository extends CrudRepository <Article, Integer> {

}
