package cda6.JCV.JB_CV_CF_SpringBraderie;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import cda6.JCV.JB_CV_CF_SpringBraderie.beans.Article;
import cda6.JCV.JB_CV_CF_SpringBraderie.service.ArticleService;
import cda6.JCV.JB_CV_CF_SpringBraderie.service.UserService;

//Test Lucas
@SpringBootApplication
@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan(basePackages = {"cda6.JCV.JB_CV_CF_SpringBraderie"})
public class App {

	/* Autre manière de se connecter à la BDD
	 * @Bean
	public DataSource dataSource() {
	    DriverManagerDataSource dataSource = new DriverManagerDataSource();

	    dataSource.setDriverClassName("org.postgresql.Driver");
	    dataSource.setUsername("lab");
	    dataSource.setPassword("lab123");
	    dataSource.setUrl(
	      "jdbc:postgresql://localhost:5432/braderie"); 

	    return dataSource;
	}*/

	@SuppressWarnings("resource")
	public static void main(String[] args){

		//Instanciation du contexte de l'application 
		ApplicationContext ctx = new AnnotationConfigApplicationContext(App.class);

		//A partir de ce contexte, récupération du service "ArticleService"
		ArticleService articleService = ctx.getBean(ArticleService.class);

		//Appel des méthodes du service Article
		articleService.getArticleById(7);
		articleService.addArticle(new Article("Samsung Galaxy S10", "Samsung", 150));

		//Puis le meilleur pour la fin :
		UserService user_service = ctx.getBean(UserService.class);
		
		//Appel de la méthode du service
		articleService.getCatalogue();
		
		//Récupérer la liste des users
		user_service.findAllUser();
		
		//Récupérer un user
		user_service.findById(1);
	}
}

